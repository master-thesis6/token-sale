import React, { useState, useEffect } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Link from "@material-ui/core/Link";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import MonetizationOnRoundedIcon from "@material-ui/icons/MonetizationOnRounded";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import getWeb3 from "./getWeb3";
import TokenSale from "./contracts/MyCrowdsale.json";
import Token from "./contracts/MyToken.json";
require("dotenv").config({ path: "../../.env" });

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://material-ui.com/">
        DundieSale
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    height: "100vh",
    backgroundColor: "black",
  },
  image: {
    backgroundImage: "url(https://images-na.ssl-images-amazon.com/images/I/81Kdgs9toDL._RI_.jpg)",
    backgroundRepeat: "no-repeat",
    backgroundColor: theme.palette.type === "light" ? theme.palette.grey[50] : theme.palette.grey[900],
    backgroundSize: "cover",
    backgroundPosition: "center",
    opacity: 0.7,
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(0),
    backgroundColor: "white",
    color: "#003366",
  },
  form: {
    width: "70%", // Fix IE 11 issue.
    marginTop: theme.spacing(11),
  },
  submit: {
    margin: theme.spacing(3, 0, 4),
    height: "50px",
    width: "50%",
    alignSelf: "center",
  },
}));

export default function SignInSide() {
  const classes = useStyles();

  const [web3Info, setWeb3Info] = useState(null);

  const [tokenError, setTokenError] = useState(false);
  const [addressError, setAddressError] = useState(false);

  const [tokenAmount, setTokenAmount] = useState(0);
  const [availableTokens, setAvailableTokens] = useState(0);
  const [address, setAddress] = useState("");

  const loadWeb3AndContracts = async () => {
    try {
      const web3 = await getWeb3();

      const accounts = await web3.eth.getAccounts();

      const networkId = await web3.eth.net.getId();

      const token = new web3.eth.Contract(Token.abi, Token.networks[networkId] && Token.networks[networkId].address);

      const tokenSale = new web3.eth.Contract(
        TokenSale.abi,
        TokenSale.networks[networkId] && TokenSale.networks[networkId].address
      );

      setWeb3Info({ web3: web3, token: token, tokenSale: tokenSale, accounts: accounts, networkId: networkId });
    } catch (error) {
      alert(`Failed to load web3, accounts, or contract. Check console for details.`);
      console.error(error);
    }
  };

  const updateAvailableTokens = async () => {
    if (web3Info == null) {
      return;
    }
    let tokens = await web3Info.token.methods.balanceOf(web3Info.tokenSale._address).call();

    setAvailableTokens(tokens);
  };

  const listenToTokenTransfer = async () => {
    if (web3Info == null) {
      return;
    }
    web3Info.token.events.Transfer({ to: web3Info.tokenSale._address }).on("data", updateAvailableTokens);
  };

  useEffect(() => {
    loadWeb3AndContracts();
  }, []);

  const onChange = (e, type) => {
    const value = e.target.value;
    if (type === "tokenAmount") {
      if (value >= 200 && value <= Number(availableTokens)) {
        setTokenAmount(value);
        setTokenError(false);
      } else {
        setTokenError(true);
      }
    } else if (type === "recipientAddress") {
      if (value === "") {
        setAddressError(true);
      } else {
        setAddress(value);
        setAddressError(false);
      }
    }
  };

  const submit = async (event) => {
    event.preventDefault();
    console.log(process.env);
    if (tokenError || addressError) {
      return;
    }
    if (tokenAmount < 200) {
      setTokenError(true);
      return;
    }

    if (address === "") {
      setAddressError(true);
      return;
    }

    await web3Info.tokenSale.methods
      .buyTokens(address)
      .send({ from: web3Info.accounts[0], value: tokenAmount * 85000000000000 });
  };
  if (web3Info == null) {
    return (
      <Typography component="h1" variant="h5">
        Loading web3 and the contracts ...
      </Typography>
    );
  }

  updateAvailableTokens();
  listenToTokenTransfer();

  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={4} md={7} className={classes.image} />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <Typography component="h1" variant="h3">
            Dundie Token
          </Typography>
          <Avatar className={classes.avatar}>
            <MonetizationOnRoundedIcon fontSize="large" />
          </Avatar>
          <Typography component="h1" variant="h5">
            Available Tokens: {availableTokens}
          </Typography>

          <Typography component="h1" variant="h6">
            The cost of Dundie: 0.000085 ETH
          </Typography>
          <form className={classes.form} noValidate onSubmit={(e) => submit(e)}>
            <Typography component="h1" variant="h5">
              Buy Dundies (min. 200)
            </Typography>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="tokenAmount"
              label="Number of tokens to buy"
              name="tokenAmount"
              error={tokenError}
              autoFocus
              onChange={(e) => onChange(e, "tokenAmount")}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="recipientAddress"
              label="Address of recipient"
              type="recipientAddress"
              error={addressError}
              id="recipientAddress"
              onChange={(e) => onChange(e, "recipientAddress")}
            />
            <Button type="submit" fullWidth variant="contained" color="primary" className={classes.submit}>
              BUY
            </Button>
            <Box mt={5}>
              <Copyright />
            </Box>
          </form>
        </div>
      </Grid>
    </Grid>
  );
}
