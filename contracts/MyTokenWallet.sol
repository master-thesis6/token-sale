pragma solidity ^0.6.0;
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract MyTokenWallet is Ownable {
    IERC20 private _token;

    constructor(IERC20 token) public {
        require(
            address(token) != address(0),
            "MyTokenWallet: token is the zero address"
        );

        _token = token;
    }

    function send(address beneficiary, uint256 tokenAmount) public onlyOwner {
        _token.transfer(beneficiary, tokenAmount);
    }

    function allow(address beneficiary, uint256 tokenAmount) public onlyOwner {
        _token.approve(beneficiary, tokenAmount);
    }

    function currentBalance() public view returns (uint256) {
        return address(this).balance;
    }

    receive() external payable {}

    function renounceOwnership() public override onlyOwner {
        revert("Can't renounce ownership here");
    }
}
