pragma solidity ^0.6.0;
import "@openzeppelin/contracts/access/Ownable.sol";

contract WalletHolder is Ownable {
    mapping(uint256 => address) private user;

    function addUser(uint256 _phoneNumber, address _walletAddress)
        public
        onlyOwner
    {
        user[_phoneNumber] = _walletAddress;
    }

    function getAddress(uint256 _phoneNumber)
        public
        view
        onlyOwner
        returns (address)
    {
        return user[_phoneNumber];
    }

    function renounceOwnership() public override onlyOwner {
        revert("Can't renounce ownership here");
    }
}
